//QGuiApplication.cs
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using Ssepan.Utility.Core;
using Ssepan.Io.Core;
using Ssepan.Application.Core;
using Ssepan.Application.Core.QmlNet;
using MvcLibrary.Core;
using Qml.Net;

namespace MvcForms.Core.QmlNet
{
	[Signal("showFileDialog", NetVariantType.Object)]
    [Signal("showFontDialog", NetVariantType.Object)]
    [Signal("showColorDialog", NetVariantType.Object)]
    [Signal("showMessageDialog", NetVariantType.Object)]
    [Signal("showAboutDialog", NetVariantType.Object)]
    [Signal("showPrinterDialog", NetVariantType.Object)]
    [Signal("propertyChanged", NetVariantType.String)]
    public class MvcView :
        // Qml.Net doesn't have a window/form that we access directly, use object where generics require parameter.
        //object, 
        // Using Qml Signals for form, but receives PropertyChanged from Model;
        // this code-behind may need to intercept the latter and trigger the former.
        INotifyPropertyChanged
    {
        #region Declarations
        protected bool disposed;
        // private bool _ValueChangedProgrammatically;
        private const string IMAGE_RESOURCE_PATH = "Resources/{0}.png"; //../MvcForms.Core.QmlNet/Resources/{0}.png"
        public const string APP_NAME = Program.APP_NAME;//"MvcForms.Core.QmlNet";

		//cancellation hook
		// Action cancelDelegate = null;
        protected MVCViewModel ViewModel;
        #endregion Declarations

        #region Constructors    
        public MvcView()
        {
            try
            {
                // _statusMessage.Text = "";
                // _errorMessage.Text = "";

                ////(re)define default output delegate
                //ConsoleApplication.defaultOutputDelegate = ConsoleApplication.messageBoxWrapperOutputDelegate;

                //subscribe to view's notifications
                if (PropertyChanged != null)
                {
                    PropertyChanged += PropertyChangedEventHandlerDelegate;
                }

                Task.Run(async () => await InitViewModel());//IDE0200 removing lambda is not clearly still async/await

                BindSizeAndLocation();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }
        #endregion Constructors

        #region INotifyPropertyChanged
        // Handle PropertyChanged from Model, ViewModel; 
        // form will fire Qml PropertyChanged signal to update view.
        public event PropertyChangedEventHandler PropertyChanged;
        // protected void OnPropertyChanged(string propertyName)
        // {
        //     try
        //     {
        //         if (this.PropertyChanged != null)
        //         {
        //             this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         ViewModel.ErrorMessage = ex.Message;
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

        //         throw;
        //     }
        // }

        //If property of object changes, call OnPropertyChanged, which notifies any subscribed observers 
        //  by firing PropertyChanged. Called by all 'set' statements in properties.
        //Implementing INotifyPropertyChanged was mostly an exercise in organizing the class as desired; 
        // this method will raise a Qml Signal. At a higher level, the intent is to intercept the PropertyChanged event
        // from Model, ViewModel, and generate a OnPropertyChanged Qml signal. Properties in the code-behind 
        // mirror the controls in the Qml.
        //public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            try
            {
                //if (this.PropertyChanged != null)
                //{
                    // Console.WriteLine("OnPropertyChanged:ActivateSignal="+propertyName);
                    //this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));//Note: this notification not recognized by Qml, ...
                    this.ActivateSignal("propertyChanged", propertyName);//Note: ...qml file will be watching for this notification instead.
                //}
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }
        #endregion INotifyPropertyChanged 

        #region Properties
        private string _ViewName = Program.APP_NAME;
        public string ViewName
        {
            get { return _ViewName; }
            set
            {
                _ViewName = value;
                OnPropertyChanged(nameof(ViewName));
            }
        }
        private string _Title = "";
        public string Title
        {
            get { return _Title; }
            set
            {
                _Title = value;
                OnPropertyChanged(nameof(Title));
            }
        }
        #endregion Properties

        // Qml can pass .NET types to .NET methods.

        #region Event Handlers
        #region PropertyChangedEventHandlerDelegates
        /// <summary>
        /// Note: This handler will intercept PropertyChanged event raised by the Model (fields), ViewModel (status bar)
        ///  and set properties that will trigger OnPropertyChanged QMl signals.
        /// Note: Dialogs class will fire PropertyChanged to be handled here
        ///  for the purpose of alerting view to fire dialog signals.
        /// Note: model property changes update UI manually.
        /// Note: handle settings property changes manually.
        /// Note: because settings properties are a subset of the model
        ///  (every settings property should be in the model,
        ///  but not every model property is persisted to settings)
        ///  it is decided that for now the settings handler will
        ///  invoke the model handler as well.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">PropertyChangedEventArgs</param>
        protected void PropertyChangedEventHandlerDelegate
        (
            object sender,
            PropertyChangedEventArgs e
        )
        {
            try
            {
                #region Dialogs
                //TODO:handle dialog-triggering events here
                if (e.PropertyName == "Message")
                {
                    // Console.WriteLine("MvcView:PropertyChangedEventHandlerDelegate:e.PropertyName=" + e.PropertyName);
                    OnShowMessageDialog(e.PropertyName);
                }
                else if (e.PropertyName == "About")
                {
                    // Console.WriteLine("MvcView:PropertyChangedEventHandlerDelegate:e.PropertyName=" + e.PropertyName);
                    OnShowAboutDialog(e.PropertyName);
                }
                else if (e.PropertyName == "Font")
                {
                    // Console.WriteLine("MvcView:PropertyChangedEventHandlerDelegate:e.PropertyName=" + e.PropertyName);
                    OnShowFontDialog(e.PropertyName);
                }
                else if (e.PropertyName == "Color")
                {
                    // Console.WriteLine("MvcView:PropertyChangedEventHandlerDelegate:e.PropertyName=" + e.PropertyName);
                    OnShowColorDialog(e.PropertyName);
                }
                else if (e.PropertyName == "File")
                {
                    // Console.WriteLine("MvcView:PropertyChangedEventHandlerDelegate:e.PropertyName=" + e.PropertyName);
                    OnShowFileDialog(e.PropertyName);
                }
                else if (e.PropertyName == "Printer")
                {
                    // Console.WriteLine("MvcView:PropertyChangedEventHandlerDelegate:e.PropertyName=" + e.PropertyName);
                    OnShowPrinterDialog(e.PropertyName);
                }
                #endregion Dialogs

                #region Model
                else if (e.PropertyName == "IsChanged")
                {
                    //ConsoleApplication.defaultOutputDelegate(string.Format("{0}", e.PropertyName));
                    ApplySettings();
                }
                //Status Bar
                else if (e.PropertyName == "StatusMessage")
                {
                    //replace default action by setting control property
                    //skip status message updates after Viewmodel is null
                    StatusMessage = (ViewModel?.StatusMessage);
                    //e = new PropertyChangedEventArgs(e.PropertyName + ".handled");

                    //ConsoleApplication.defaultOutputDelegate(string.Format("{0}", StatusMessage));
                }
                else if (e.PropertyName == "ErrorMessage")
                {
                    //replace default action by setting control property
                    //skip status message updates after Viewmodel is null
                    ErrorMessage = (ViewModel?.ErrorMessage);
                    //e = new PropertyChangedEventArgs(e.PropertyName + ".handled");

                    //ConsoleApplication.defaultOutputDelegate(string.Format("{0}", ErrorMessage));
                }
                else if (e.PropertyName == "CustomMessage")
                {
                    //replace default action by setting control property
                    //StatusBarCustomMessage.Text = ViewModel.CustomMessage;
                    //e = new PropertyChangedEventArgs(e.PropertyName + ".handled");

                    //ConsoleApplication.defaultOutputDelegate(string.Format("{0}", ErrorMessage));
                }
                else if (e.PropertyName == "ErrorMessageToolTipText")
                {
                    ErrorMessageToolTipText = ViewModel.ErrorMessageToolTipText;
                }
                else if (e.PropertyName == "ProgressBarValue")
                {
                    ProgressBarValue = ViewModel.ProgressBarValue / 100;
                }
                else if (e.PropertyName == "ProgressBarMaximum")
                {
                    ProgressBarMaximum = ViewModel.ProgressBarMaximum;
                }
                else if (e.PropertyName == "ProgressBarMinimum")
                {
                    ProgressBarMinimum = ViewModel.ProgressBarMinimum;
                }
                else if (e.PropertyName == "ProgressBarStep")
                {
                    ProgressBarStep = ViewModel.ProgressBarStep;
                }
                else if (e.PropertyName == "ProgressBarIsMarquee")
                {
                    ProgressBarIsMarquee = ViewModel.ProgressBarIsMarquee;
                }
                else if (e.PropertyName == "ProgressBarIsVisible")
                {
                    ProgressBarIsVisible = ViewModel.ProgressBarIsVisible;
                }
                else if (e.PropertyName == "ActionIconIsVisible")
                {
                    ActionIconIsVisible = ViewModel.ActionIconIsVisible;
                }
                else if (e.PropertyName == "ActionIconImage")
                {
                    // Console.WriteLine("PropertyChangedEventHandlerDelegate:ActionIconImage=" + ActionIconImage);
                    ActionIconImage =

                            ViewModel != null
                            ?
                            (
                                ViewModel.ActionIconImage ?? ViewModel.GetActionIconInfo("App")
                            )
                            :
                            string.Format(IMAGE_RESOURCE_PATH, "App") /*(string)null*/
                        ;
                }
                else if (e.PropertyName == "DirtyIconIsVisible")
                {
                    DirtyIconIsVisible = ViewModel.DirtyIconIsVisible;
                }
                else if (e.PropertyName == "DirtyIconImage")
                {
                    DirtyIconImage = ViewModel.DirtyIconImage;
                }
                //use if properties cannot be databound
                else if (e.PropertyName == "SomeInt")
                {
                   SomeInt = ModelController<MVCModel>.Model.SomeInt;
                }
                else if (e.PropertyName == "SomeBoolean")
                {
                   SomeBoolean = ModelController<MVCModel>.Model.SomeBoolean;
                }
                else if (e.PropertyName == "SomeString")
                {
                   SomeString = ModelController<MVCModel>.Model.SomeString;
                }
                else if (e.PropertyName == "StillAnotherInt")
                {
                   StillAnotherInt = ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherInt;
                }
                else if (e.PropertyName == "StillAnotherBoolean")
                {
                   StillAnotherBoolean = ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean;
                }
                else if (e.PropertyName == "StillAnotherString")
                {
                   StillAnotherString = ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherString;
                }
                else if (e.PropertyName == "SomeOtherInt")
                {
                   SomeOtherInt = ModelController<MVCModel>.Model.SomeComponent.SomeOtherInt;
                }
                else if (e.PropertyName == "SomeOtherBoolean")
                {
                   SomeOtherBoolean = ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean;
                }
                else if (e.PropertyName == "SomeOtherString")
                {
                   SomeOtherString = ModelController<MVCModel>.Model.SomeComponent.SomeOtherString;
                }
                //else if (e.PropertyName == "SomeComponent")
                //{
                //    ConsoleApplication.defaultOutputDelegate(string.Format("SomeComponent: {0},{1},{2}", ModelController<MVCModel>.Model.SomeComponent.SomeOtherInt, ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean, ModelController<MVCModel>.Model.SomeComponent.SomeOtherString));
                //}
                //else if (e.PropertyName == "StillAnotherComponent")
                //{
                //    ConsoleApplication.defaultOutputDelegate(string.Format("StillAnotherComponent: {0},{1},{2}", ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherInt, ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean, ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherString));
                //}
                else
                {
                    #if DEBUG_MODEL_PROPERTYCHANGED
                    ConsoleApplication.defaultOutputDelegate(string.Format("e.PropertyName: {0}", e.PropertyName));
                    #endif
                }
                #endregion Model

                #region Settings
                if (e.PropertyName == "Dirty")
                {
                    //apply settings that don't have databindings
                    ViewModel.DirtyIconIsVisible = SettingsController<MVCSettings>.Settings.Dirty;
                }
                else
                {
                    #if DEBUG_SETTINGS_PROPERTYCHANGED
                    ConsoleApplication.defaultOutputDelegate(string.Format("e.PropertyName: {0}", e.PropertyName));
                    #endif
                }
                #endregion Settings
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        #endregion PropertyChangedEventHandlerDelegates

        #region Form
        //Load
        public void Window_Loaded(object sender, EventArgs e)
        {
            try
            {
                ViewModel.StatusMessage = string.Format("{0} starting...", ViewName);

                // _Run();//only called here in console apps; use button in forms apps
                ModelController<MVCModel>.Model.Refresh();//mirror of onCompleted event in QMl

                ViewModel.StatusMessage = string.Format("{0} started.", ViewName);
            }
            catch (Exception ex)
            {
                ViewModel.ErrorMessage = ex.Message;
                ViewModel.StatusMessage = string.Empty;

                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        //Closed
        /// <summary>
        /// handles Window_Delete and (indirectly) FileQuit_Clicked
        /// </summary>
        // private async void Window_Closed(object sender, /*Cancel*/EventArgs e)
        // {
        //     bool resultDontQuit = false;

        //     try
        //     {
        //         await ViewModel.FileExit(ref resultDontQuit);
        //         e.RetVal = resultDontQuit;
        //         if (!resultDontQuit)//use this in FileQuitAction()
        //         {
        //             ViewModel.StatusMessage = string.Format("{0} completing...", ViewName);
        //             await DisposeSettings();
        //             ViewModel.StatusMessage = string.Format("{0} completed.", ViewName);

        //             ViewModel = null;

        //             Application.Quit();
        //         }
        //     }
        //     catch(Exception ex)
        //     {
        //         ViewModel.ErrorMessage = ex.Message.ToString();
        //         ViewModel.StatusMessage = "";

        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //     }
        // }

        // private void Window_KeyPressEvent(object sender, /*KeyPress*/EventArgs e)
        // {
        // //     bool returnValue = default(bool);
        //     try
        //     {

        //         // Implement the Escape / Cancel keystroke
        //         // if (keyData == Keys.Cancel || keyData == Keys.Escape)
        //         // {
        //         //     //if a long-running cancellable-action has registered 
        //         //     //an escapable-event, trigger it
        //         //     InvokeActionCancel();

        //         //     // This keystroke was handled, 
        //         //     //don't pass to the control with the focus
        //         //     returnValue = true;
        //         // }
        //         // else
        //         // {
        //         //     returnValue = base.ProcessCmdKey(ref msg, keyData);
        //         // }

        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //     }
        // //     return returnValue;
        // }
        #endregion Form

        #region Buttons
        public void cmdRun_Clicked(object sender)//IDE0060 not appropriate, ignore
        {
            ViewModel.DoSomething();
        }
        public async void cmdFont_Clicked(object sender)
        {
            await ViewModel.GetFont();
        }
        public async void cmdColor_Clicked(object sender)
        {
            await ViewModel.GetColor();
        }
        #endregion Buttons

        #region Menu/Toolbar
        public async void FileNew_Triggered(object sender)
        {
            await ViewModel.FileNew();
        }
        public async void FileOpen_Triggered(object sender)
        {
            await ViewModel.FileOpen();
        }
        public async void FileSave_Triggered(object sender)
        {
            await ViewModel.FileSave();
        }
        public async void FileSaveAs_Triggered(object sender)
        {
            await ViewModel.FileSaveAs();
        }
        public async void FilePrint_Triggered(object sender)
        {
            await ViewModel.FilePrint();
        }
        public async void FilePrintPreview_Triggered(object sender)
        {
            await ViewModel.FilePrintPreview();
        }
        // public void FileQuit_Triggered(object sender)
        // {
        //     //TODO:Close() should be called in qml; this, or related action, might be called to do checks and return bool
        //     //Console.WriteLine("MenuFileQuit_Triggered");
        // }
        public async void EditUndo_Triggered(object sender)
        {
            await ViewModel.EditUndo();
        }
        public async void EditRedo_Triggered(object sender)
        {
            await ViewModel.EditRedo();
        }
        public async void EditSelectAll_Triggered(object sender)
        {
            await ViewModel.EditSelectAll();
        }
        public async void EditCut_Triggered(object sender)
        {
            await ViewModel.EditCut();
        }
        public async void EditCopy_Triggered(object sender)
        {
            await ViewModel.EditCopy();
        }
        public async void EditPaste_Triggered(object sender)
        {
            await ViewModel.EditPaste();
        }
        public async void EditPasteSpecial_Triggered(object sender)
        {
            await ViewModel.EditPasteSpecial();
        }
        public async void EditDelete_Triggered(object sender)
        {
            await ViewModel.EditDelete();
        }
        public async void EditFind_Triggered(object sender)
        {
            await ViewModel.EditFind();
        }
        public async void EditReplace_Triggered(object sender)
        {
            await ViewModel.EditReplace();
        }
        public async void EditRefresh_Triggered(object sender)
        {
            await ViewModel.EditRefresh();
        }
        public async void EditPreferences_Triggered(object sender)
        {
            await ViewModel.EditPreferences();
        }
        public async void EditProperties_Triggered(object sender)
        {
            await ViewModel.EditProperties();
        }
        public async void WindowNewWindow_Triggered(object sender)
        {
            await ViewModel.WindowNewWindow();
        }
        public async void WindowTile_Triggered(object sender)
        {
            await ViewModel.WindowTile();
        }
        public async void WindowCascade_Triggered(object sender)
        {
            await ViewModel.WindowCascade();
        }
        public async void WindowArrangeAll_Triggered(object sender)
        {
            await ViewModel.WindowArrangeAll();
        }
        public async void WindowHide_Triggered(object sender)
        {
            await ViewModel.WindowHide();
        }
        public async void WindowShow_Triggered(object sender)
        {
            await ViewModel.WindowShow();
        }
        public async void HelpContents_Triggered(object sender)
        {
            await ViewModel.HelpContents();
        }
        public async void HelpIndex_Triggered(object sender)
        {
            await ViewModel.HelpIndex();
        }
        public async void HelpOnlineHelp_Triggered(object sender)
        {
            await ViewModel.HelpOnlineHelp();
        }
        public async void HelpLicenceInformation_Triggered(object sender)
        {
            await ViewModel.HelpLicenceInformation();
        }
        public async void HelpCheckForUpdates_Triggered(object sender)
        {
            await ViewModel.HelpCheckForUpdates();
        }
        public async void HelpAbout_Triggered(object sender)
        {
            await ViewModel.HelpAbout<AssemblyInfo>();
        }
        #endregion Menu/Toolbar
        #endregion Event Handlers

        #region Methods
        #region FormAppBase
        protected async Task InitViewModel()
        {
            FileDialogInfo<object, string> settingsFileDialogInfo = null;

            try
            {
                //tell controller how model should notify view about non-persisted properties AND including model properties that may be part of settings
                ModelController<MVCModel>.DefaultHandler = PropertyChangedEventHandlerDelegate;

                //tell controller how settings should notify view about persisted properties
                SettingsController<MVCSettings>.DefaultHandler = PropertyChangedEventHandlerDelegate;

                //Dialogs class needs to alert Qml view when dialogs must be displayed;
                // an event from Dialogs will result in a Qml signal from the view code-behind.
                DialogsController.Dialogs.Instance.DefaultHandler = PropertyChangedEventHandlerDelegate;

                InitModelAndSettings();

				//settings used with file dialog interactions
				settingsFileDialogInfo =
					new FileDialogInfo<object, string>
					(
						parent: this,
						modal: true,
						title: null,
						response: FormsViewModel<string, MVCSettings, MVCModel, MvcView/*object*/>.Button_None,
						newFilename: SettingsController<MVCSettings>.FILE_NEW,
						filename: null,
						extension: SettingsBase.FileTypeExtension,
						description: SettingsBase.FileTypeDescription,
						typeName: SettingsBase.FileTypeName,
						additionalFilters:
						[
                            // "MvcSettings files (*.mvcsettings)", 
                            // "JSON files (*.json)", 
                            // "XML files (*.xml)", 
                            // "All files (*.*)" 
                            string.Format(DialogsController.Dialogs.FILTER_FORMAT, "MvcSettings files", "mvcsettings"),
							string.Format(DialogsController.Dialogs.FILTER_FORMAT, "JSON files", "json"),
							string.Format(DialogsController.Dialogs.FILTER_FORMAT, "XML files", "xml"),
							string.Format(DialogsController.Dialogs.FILTER_FORMAT, "All files", "*")//, 
                        ],
						multiselect: false,
						initialDirectory: default,
						forceDialog: false,
						forceNew: false,
						customInitialDirectory: Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator()
					)
					{
						//set dialog caption
						Title = ViewName
					};

				//class to handle standard behaviors
				ViewModelController<string, MVCViewModel>.New
                (
                    ViewName,
                    new MVCViewModel
                    (
                        PropertyChangedEventHandlerDelegate,
                        new Dictionary<string, string>()
                        { //Note:ideally, should get these from library, but items added did not get generated into resource class.
                            { "App", string.Format(IMAGE_RESOURCE_PATH, "App") },
                            { "New", string.Format(IMAGE_RESOURCE_PATH, "New") },
                            { "Open", string.Format(IMAGE_RESOURCE_PATH, "Open") },
                            { "Save", string.Format(IMAGE_RESOURCE_PATH, "Save") },
                            { "SaveAs", string.Format(IMAGE_RESOURCE_PATH, "SaveAs") },
                            { "Print", string.Format(IMAGE_RESOURCE_PATH, "Print") },
                            { "PrintPreview", string.Format(IMAGE_RESOURCE_PATH, "PrintPreview") },
                            { "Quit", string.Format(IMAGE_RESOURCE_PATH, "Quit") },
                            { "Undo", string.Format(IMAGE_RESOURCE_PATH, "Undo") },
                            { "Redo", string.Format(IMAGE_RESOURCE_PATH, "Redo") },
                            { "SelectAll", string.Format(IMAGE_RESOURCE_PATH, "SelectAll") },
                            { "Cut", string.Format(IMAGE_RESOURCE_PATH, "Cut") },
                            { "Copy", string.Format(IMAGE_RESOURCE_PATH, "Copy") },
                            { "Paste", string.Format(IMAGE_RESOURCE_PATH, "Paste") },
                            { "Delete", string.Format(IMAGE_RESOURCE_PATH, "Delete") },
                            { "Find", string.Format(IMAGE_RESOURCE_PATH, "Find") },
                            { "Replace", string.Format(IMAGE_RESOURCE_PATH, "Replace") },
                            { "Refresh", string.Format(IMAGE_RESOURCE_PATH, "Reload") },
                            { "Preferences", string.Format(IMAGE_RESOURCE_PATH, "Preferences") },
                            { "Properties", string.Format(IMAGE_RESOURCE_PATH, "Properties") },
                            { "Contents", string.Format(IMAGE_RESOURCE_PATH, "Contents") },
                            { "About", string.Format(IMAGE_RESOURCE_PATH, "About") }
                            //TODO:make a way to get these back out of VM *from view*
                        },
                        settingsFileDialogInfo,
                        this
                    )
                );

                //select a viewmodel by view name
                ViewModel = ViewModelController<string, MVCViewModel>.ViewModel[ViewName];

                // BindFormUi();

                //Init config parameters
                if (!LoadParameters())
                {
                    throw new Exception(string.Format("Unable to load config file parameter(s)."));
                }

                //Load
                //BUG:these aren't showing up in view; maybe because view isn't shown yet?
                if ((SettingsController<MVCSettings>.FilePath == null) || (SettingsController<MVCSettings>.Filename == SettingsController<MVCSettings>.FILE_NEW))
                {
                    //NEW
                    await ViewModel.FileNew();
                }
                else
                {
                    //OPEN
                    await ViewModel.FileOpen(false);
                }

// #if debug
//             //debug view
//             menuEditProperties_Click(sender, e);
// #endif

                //Display dirty state
                ModelController<MVCModel>.Model.Refresh();
            }
            catch (Exception ex)
            {
                if (ViewModel != null)
                {
                    ViewModel.ErrorMessage = ex.Message;
                }
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        protected void InitModelAndSettings()
        {
            //create Settings before first use by Model
            if (SettingsController<MVCSettings>.Settings == null)
            {
                SettingsController<MVCSettings>.New();
            }
            //Model properties rely on Settings, so don't call Refresh before this is run.
            if (ModelController<MVCModel>.Model == null)
            {
                ModelController<MVCModel>.New();
            }
        }

        protected async Task DisposeSettings()
        {
            MessageDialogInfo<object, string, object, string, string> questionMessageDialogInfo = null;

            //save user and application settings 
            Properties.Settings.Default.Save();

            if (SettingsController<MVCSettings>.Settings.Dirty)
            {
                //prompt before saving
                questionMessageDialogInfo = new MessageDialogInfo<object, string, object, string, string>
                (
                    this,
                    true,
                    "Save",
                    null,
                    "Question",
                    "YesNo",
                    "Save changes?",
                    MVCViewModel.Button_None
                );

                questionMessageDialogInfo = await DialogsController.Dialogs.Instance.ShowMessageDialog(questionMessageDialogInfo);

                if (!questionMessageDialogInfo.BoolResult)
                {
                    throw new ApplicationException("Save question dialog returned with an error");
                }

                // DialogResult dialogResult = MessageBox.Show("Save changes?", this.Title, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                switch (questionMessageDialogInfo.Response)
                {
					case MVCViewModel.Button_Yes:
						//SAVE
						await ViewModel.FileSave();

						break;

					case MVCViewModel.Button_No:
						break;

					default:
						throw new InvalidEnumArgumentException();
				}
            }

            //unsubscribe from model notifications
            ModelController<MVCModel>.Model.PropertyChanged -= PropertyChangedEventHandlerDelegate;
        }

        // protected void Run()
        // {
        //     //MessageBox.Show("running", "MVC", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
        // }
        #endregion FormAppBase

        #region Utility
        // /// <summary>
        // /// Bind static Model controls to Model Controller
        // /// </summary>
        // private void BindFormUi()
        // {
        //     try
        //     {
        //         //Form

        //         //Controls
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //         throw;
        //     }
        // }

        // /// <summary>
        // /// Bind Model controls to Model Controller
        // /// Note: databinding not available in GtkSharp, but leave this in place
        // ///  in case I figure out a way to do this
        // /// </summary>
        // private void BindModelUi()
        // {
        //     try
        //     {
        //         // BindField<Entry, MVCModel>(_txtSomeInt, ModelController<MVCModel>.Model, "SomeInt");
        //         // BindField<Entry, MVCModel>(_txtSomeString, ModelController<MVCModel>.Model, "SomeString");
        //         // BindField<CheckButton, MVCModel>(_chkSomeBoolean, ModelController<MVCModel>.Model, "SomeBoolean", "Checked");

        //         // BindField<Entry, MVCModel>(_txtSomeOtherInt, ModelController<MVCModel>.Model, "SomeComponent.SomeOtherInt");
        //         // BindField<Entry, MVCModel>(_txtSomeOtherString, ModelController<MVCModel>.Model, "SomeComponent.SomeOtherString");
        //         // BindField<CheckButton, MVCModel>(_chkSomeOtherBoolean, ModelController<MVCModel>.Model, "SomeComponent.SomeOtherBoolean", "Checked");

        //         // BindField<Entry, MVCModel>(_txtStillAnotherInt, ModelController<MVCModel>.Model, "StillAnotherComponent.StillAnotherInt");
        //         // BindField<Entry, MVCModel>(_txtStillAnotherString, ModelController<MVCModel>.Model, "StillAnotherComponent.StillAnotherString");
        //         // BindField<CheckButton, MVCModel>(_chkStillAnotherBoolean, ModelController<MVCModel>.Model, "StillAnotherComponent.StillAnotherBoolean", "Checked");
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //         throw;
        //     }
        // }

        // /// Note: databinding not available in GtkSharp, but leave this in place 
        // ///  in case I figure out a way to do this
        // private void BindField<TControl, TModel>
        // (
        //     TControl fieldControl,
        //     TModel model,
        //     string modelPropertyName,
        //     string controlPropertyName = "Text",
        //     bool formattingEnabled = false,
        //     //DataSourceUpdateMode dataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged,
        //     bool reBind = true
        // )
        //     //where TControl : Qml.Widget
        // {
        //     try
        //     {
        //         //TODO: .RemoveSignalHandler ?
        //         //fieldControl.DataBindings.Clear();
        //         if (reBind)
        //         {
        //             //TODO:.AddSignalHandler ?
        //             //fieldControl.DataBindings.Add(controlPropertyName, model, modelPropertyName, formattingEnabled, dataSourceUpdateMode);
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //     }
        // }

        /// <summary>
        /// Apply Settings to viewer.
        /// </summary>
        private void  ApplySettings()
        {
            try
            {
                // _ValueChangedProgrammatically = true;

                //apply settings that have databindings
                // BindModelUi();

                //apply settings that shouldn't use databindings

                //apply settings that can't use databindings
                Title = System.IO.Path.GetFileName(SettingsController<MVCSettings>.Filename) + " - " + ViewName;

                //apply settings that don't have databindings
                ViewModel.DirtyIconIsVisible = SettingsController<MVCSettings>.Settings.Dirty;
                //update remaining non-bound controls: textboxes, checkboxes
                //Note: ModelController<MVCModel>.Model.Refresh() will cause SO here
                ModelController<MVCModel>.Model.Refresh(nameof(SomeInt));
                ModelController<MVCModel>.Model.Refresh(nameof(SomeBoolean));
                ModelController<MVCModel>.Model.Refresh(nameof(SomeString));
                ModelController<MVCModel>.Model.Refresh(nameof(SomeOtherInt));
                ModelController<MVCModel>.Model.Refresh(nameof(SomeOtherBoolean));
                ModelController<MVCModel>.Model.Refresh(nameof(SomeOtherString));
                ModelController<MVCModel>.Model.Refresh(nameof(StillAnotherInt));
                ModelController<MVCModel>.Model.Refresh(nameof(StillAnotherBoolean));
                ModelController<MVCModel>.Model.Refresh(nameof(StillAnotherString));

                // _ValueChangedProgrammatically = false;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
        }

        // /// <summary>
        // /// Set function button and menu to enable value, and cancel button to opposite.
        // /// For now, do only disabling here and leave enabling based on biz logic 
        // ///  to be triggered by refresh?
        // /// </summary>
        // /// <param name="functionButton"></param>
        // /// <param name="functionMenu"></param>
        // /// <param name="cancelButton"></param>
        // /// <param name="enable"></param>
        // private void SetFunctionControlsEnable
        // (
        //     Button functionButton,
        //     ToolButton functionToolbarButton,
        //     ImageMenuItem functionMenu,
        //     Button cancelButton,
        //     bool enable
        // )
        // {
        //     try
        //     {
        //         //stand-alone button
        //         if (functionButton != null)
        //         {
        //             functionButton.Sensitive = enable;
        //         }

        //         //toolbar button
        //         if (functionToolbarButton != null)
        //         {
        //             functionToolbarButton.Sensitive = enable;
        //         }

        //         //menu item
        //         if (functionMenu != null)
        //         {
        //             functionMenu.Sensitive = enable;
        //         }

        //         //stand-alone cancel button
        //         if (cancelButton != null)
        //         {
        //             cancelButton.Sensitive = !enable;
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //     }
        // }

        // /// <summary>
        // /// Invoke any delegate that has been registered 
        // ///  to cancel a long-running background process.
        // /// </summary>
        // private void InvokeActionCancel()
        // {
        //     try
        //     {
        //         //execute cancellation hook
        //         if (cancelDelegate != null)
        //         {
        //             cancelDelegate();
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //     }
        // }

        /// <summary>
        /// Load from app config; override with command line if present
        /// </summary>
        /// <returns>bool</returns>
        private bool LoadParameters()
        {
            bool returnValue = default;

            try
            {
                // First, get configured values

                //get filename from App.config
                if (!Configuration.ReadString("SettingsFilename", out string _settingsFilename))
                {
                    throw new ApplicationException(string.Format("Unable to load SettingsFilename: {0}", "SettingsFilename"));
                }
                if ((_settingsFilename == null) || (_settingsFilename == SettingsController<MVCSettings>.FILE_NEW))
                {
                    throw new ApplicationException(string.Format("Settings filename not set: '{0}'.\nCheck SettingsFilename in App.config file.", _settingsFilename));
                }
                //use with the supplied path
                SettingsController<MVCSettings>.Filename = _settingsFilename;

                //identify directory to specified, or use default
                if (Path.GetDirectoryName(_settingsFilename)?.Length == 0)
                {
                    //supply default path if missing
                    SettingsController<MVCSettings>.Pathname = Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator();
                }

                //get serialization format from App.config
                if (!Configuration.ReadString("SettingsSerializeAs", out string _settingsSerializeAs))
                {
                    throw new ApplicationException(string.Format("Unable to load SettingsSerializeAs: {0}", "SettingsSerializeAs"));
                }
                if (string.IsNullOrEmpty(_settingsSerializeAs))
                {
                    throw new ApplicationException(string.Format("Settings serialization format not set: '{0}'.\nCheck SettingsSerializeAs in App.config file.", _settingsSerializeAs));
                }
                //use with the filename
                SettingsBase.SerializeAs = SettingsBase.ToSerializationFormat(_settingsSerializeAs);

                //Second, override with passed values

                if ((Program.Filename != default) && (Program.Filename != SettingsController<MVCSettings>.FILE_NEW))
                {
                    //got filename from command line
                    SettingsController<MVCSettings>.Filename = Program.Filename;
                }
                if (Program.Directory != default)
                {
                    //get default directory from command line
                    SettingsController<MVCSettings>.Pathname = Program.Directory.WithTrailingSeparator();
                }
                if (Program.Format != default)
                {
                    // get default format from command line
                    SettingsBase.SerializeAs = Program.Format switch
                    {
                        "xml" => SettingsBase.SerializationFormat.Xml,
                        "json" => SettingsBase.SerializationFormat.Json,
                        _ => default
                    };
                }

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                //throw;
            }
            return returnValue;
        }

        private void BindSizeAndLocation()
        {

            //Note:Size must be done after InitializeComponent(); do Location this way as well.--SJS
            // this.DataBindings.Add(new System.Windows.Forms.Binding("Location", global::MVCForms.Properties.Settings.Default, "Location", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            // this.DataBindings.Add(new System.Windows.Forms.Binding("ClientSize", global::MVCForms.Properties.Settings.Default, "Size", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            // this.SetDefaultSize(global::MvcForms.Core.QmlNet.Properties.Settings.Default.Size.Width,global::MvcForms.Core.QmlNet.Properties.Settings.Default.Size.Height);
            // this.SetPosition(WindowPosition.Center); // global::MVCForms.Properties.Settings.Default.Location;
        }

        // private async Task DoSomething()
        // {
        //     for (int i = 0; i < 3; i++)
        //     {
        //         //TODO:DoEvents...
        //         await Task.Delay(1000);
        //     }
        // }
        #endregion Utility
        #endregion Methods

        #region Qml.Net
        #region DialogInfo
        #region Methods
        //Note: These will still fire signal from here; 
        // dialog info properties will reside in Dialogs.cs
        protected void OnShowMessageDialog(string dialogType)
        {
            try
            {
                // Console.WriteLine("OnShowMessageDialog:" + dialogInfoType);
                //qml file will be watching for onShowMessageDialog
                this.ActivateSignal("showMessageDialog", dialogType);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }
        protected void OnShowAboutDialog(string dialogType)
        {
            try
            {
                //Console.WriteLine("OnShowAboutDialog:" + dialogInfoType);
                //qml file will be watching for onShowAboutDialog
                this.ActivateSignal("showAboutDialog", dialogType);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }
        protected void OnShowFontDialog(string dialogType)
        {
            try
            {
                //Console.WriteLine("OnShowFontDialog:" + dialogInfoType);
                //qml file will be watching for onShowFontDialog
                this.ActivateSignal("showFontDialog", dialogType);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }
        protected void OnShowColorDialog(string dialogType)
        {
            try
            {
                //Console.WriteLine("OnShowColorDialog:" + dialogInfoType);
                //qml file will be watching for onShowColorDialog
                this.ActivateSignal("showColorDialog", dialogType);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }
        protected void OnShowFileDialog(string dialogType)
        {
            try
            {
                //Console.WriteLine("OnShowFileDialog:" + dialogInfoType);
                //qml file will be watching for onShowFileDialog
                this.ActivateSignal("showFileDialog", dialogType);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }
        protected void OnShowPrinterDialog(string dialogType)
        {
            try
            {
                //Console.WriteLine("OnShowPrinterDialog:" + dialogInfoType);
                //qml file will be watching for onShowPrinterDialog
                this.ActivateSignal("showPrinterDialog", dialogType);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }
        #endregion Methods

        #region Properties
        // Note: Like prototype, view will still fire signal from here; 
        // however, dialog info properties will reside in Dialogs.cs 
        // and be accessed from Qml as dialogs.xxxDialogInfo.
        // public MessageDialogInfo<object, string, object, string, string> MsgDialogInfo
        // public AboutDialogInfo<object, string, string> AbtDialogInfo
        // public FontDialogInfo<object, string, string> FntDialogInfo
        // public ColorDialogInfo<object, string, string> ClrDialogInfo
        // public FileDialogInfo<object, string> FilDialogInfo
        // public PrinterDialogInfo<object, string, object> PrtDialogInfo
        #endregion DialogInfo
        #endregion Properties

        #region Model
        #region Properties
        //Note:normally I wouldn't get this elaborate for the demo, 
        // but properties and notifications seem to be necessary 
        // for interfacing with Qml / Qml.Net
        #region Fields
        private int _SomeInt;
        public int SomeInt
        {
            get { return _SomeInt; }
            set
            {
                _SomeInt = value;
                OnPropertyChanged(nameof(SomeInt));
                // Console.WriteLine("SomeInt:"+value.ToString());
            }
        }

        private bool _SomeBoolean;
        public bool SomeBoolean
        {
            get { return _SomeBoolean; }
            set
            {
                _SomeBoolean = value;
                OnPropertyChanged(nameof(SomeBoolean));
                // Console.WriteLine("SomeBoolean:"+value.ToString());
            }
        }

        private string _SomeString = string.Empty;
        public string SomeString
        {
            get { return _SomeString; }
            set
            {
                _SomeString = value;
                OnPropertyChanged(nameof(SomeString));
                // Console.WriteLine("SomeString:"+value.ToString());
            }
        }

        private int _SomeOtherInt;
        public int SomeOtherInt
        {
            get { return _SomeOtherInt; }
            set
            {
                _SomeOtherInt = value;
                OnPropertyChanged(nameof(SomeOtherInt));
                // Console.WriteLine("SomeOtherInt:"+value.ToString());
            }
        }

        private bool _SomeOtherBoolean;
        public bool SomeOtherBoolean
        {
            get { return _SomeOtherBoolean; }
            set
            {
                _SomeOtherBoolean = value;
                OnPropertyChanged(nameof(SomeOtherBoolean));
                // Console.WriteLine("SomeOtherBoolean:"+value.ToString());
            }
        }

        private string _SomeOtherString = string.Empty;
        public string SomeOtherString
        {
            get { return _SomeOtherString; }
            set
            {
                _SomeOtherString = value;
                OnPropertyChanged(nameof(SomeOtherString));
                // Console.WriteLine("SomeOtherString:"+value.ToString());
            }
        }

        private int _StillAnotherInt;
        public int StillAnotherInt
        {
            get { return _StillAnotherInt; }
            set
            {
                _StillAnotherInt = value;
                OnPropertyChanged(nameof(StillAnotherInt));
                // Console.WriteLine("StillAnotherInt:"+value.ToString());
            }
        }

        private bool _StillAnotherBoolean;
        public bool StillAnotherBoolean
        {
            get { return _StillAnotherBoolean; }
            set
            {
                _StillAnotherBoolean = value;
                OnPropertyChanged(nameof(StillAnotherBoolean));
                // Console.WriteLine("StillAnotherBoolean:"+value.ToString());
            }
        }

        private string _StillAnotherString = string.Empty;
        public string StillAnotherString
        {
            get { return _StillAnotherString; }
            set
            {
                _StillAnotherString = value;
                OnPropertyChanged(nameof(StillAnotherString));
                // Console.WriteLine("StillAnotherString:"+value.ToString());
            }
        }
        #endregion Fields
        #endregion Properties
        #endregion Model

        #region ViewModel
        #region Properties
        private string _StatusMessage;
        public string StatusMessage
        {
            get { return _StatusMessage; }
            set
            {
                if (value != _StatusMessage)
                {
                    _StatusMessage = value;
                    OnPropertyChanged(nameof(StatusMessage));
                }
            }
        }

        private string _ErrorMessage;
        public string ErrorMessage
        {
            get { return _ErrorMessage; }
            set
            {
                if (value != _ErrorMessage)
                {
                    _ErrorMessage = value;
                    OnPropertyChanged(nameof(ErrorMessage));
                }
            }
        }

        private string _CustomMessage;
        public string CustomMessage
        {
            get { return _CustomMessage; }
            set
            {
                if (value != _CustomMessage)
                {
                    _CustomMessage = value;
                    OnPropertyChanged(nameof(CustomMessage));
                }
            }
        }

        private string _ErrorMessageToolTipText;
        public string ErrorMessageToolTipText
        {
            get { return _ErrorMessageToolTipText; }
            set
            {
                if (value != _ErrorMessageToolTipText)
                {
                    _ErrorMessageToolTipText = value;
                    OnPropertyChanged(nameof(ErrorMessageToolTipText));
                }
            }
        }

        private int _ProgressBarValue;
        public int ProgressBarValue
        {
            get { return _ProgressBarValue; }
            set
            {
                if (value != _ProgressBarValue)
                {
                    _ProgressBarValue = value;
                    OnPropertyChanged(nameof(ProgressBarValue));
                }
            }
        }

        private int _ProgressBarMaximum;
        public int ProgressBarMaximum
        {
            get { return _ProgressBarMaximum; }
            set
            {
                if (value != _ProgressBarMaximum)
                {
                    _ProgressBarMaximum = value;
                    OnPropertyChanged(nameof(ProgressBarMaximum));
                }
            }
        }

        private int _ProgressBarMinimum;
        public int ProgressBarMinimum
        {
            get { return _ProgressBarMinimum; }
            set
            {
                if (value != _ProgressBarMinimum)
                {
                    _ProgressBarMinimum = value;
                    OnPropertyChanged(nameof(ProgressBarMinimum));
                }
            }
        }

        private int _ProgressBarStep;
        public int ProgressBarStep
        {
            get { return _ProgressBarStep; }
            set
            {
                if (value != _ProgressBarStep)
                {
                    _ProgressBarStep = value;
                    OnPropertyChanged(nameof(ProgressBarStep));
                }
            }
        }

        private bool _ProgressBarIsMarquee;
        public bool ProgressBarIsMarquee
        {
            get { return _ProgressBarIsMarquee; }
            set
            {
                if (value != _ProgressBarIsMarquee)
                {
                    _ProgressBarIsMarquee = value;
                    OnPropertyChanged(nameof(ProgressBarIsMarquee));
                }
            }
        }

        private bool _ProgressBarIsVisible;
        public bool ProgressBarIsVisible
        {
            get { return _ProgressBarIsVisible; }
            set
            {
                if (value != _ProgressBarIsVisible)
                {
                    _ProgressBarIsVisible = value;
                    OnPropertyChanged(nameof(ProgressBarIsVisible));
                }
            }
        }

        private bool _ActionIconIsVisible;
        public bool ActionIconIsVisible
        {
            get { return _ActionIconIsVisible; }
            set
            {
                if (value != _ActionIconIsVisible)
                {
                    _ActionIconIsVisible = value;
                    OnPropertyChanged(nameof(ActionIconIsVisible));
                }
            }
        }

        private string _ActionIconImage;
        public string ActionIconImage
        {
            get { return _ActionIconImage; }
            set
            {
                if (value != _ActionIconImage)
                {
                    _ActionIconImage = value;
                    OnPropertyChanged(nameof(ActionIconImage));
                }
            }
        }

        private bool _DirtyIconIsVisible;
        public bool DirtyIconIsVisible
        {
            get { return _DirtyIconIsVisible; }
            set
            {
                if (value != _DirtyIconIsVisible)
                {
                    _DirtyIconIsVisible = value;
                    OnPropertyChanged(nameof(DirtyIconIsVisible));
                }
            }
        }

        private string _DirtyIconImage;
        public string DirtyIconImage
        {
            get { return _DirtyIconImage; }
            set
            {
                if (value != _DirtyIconImage)
                {
                    _DirtyIconImage = value;
                    OnPropertyChanged(nameof(DirtyIconImage));
                }
            }
        }

        #endregion Properties

        #region Methods
        #region Actions

        /// <summary>
        /// Returns true = Yes = Quit
        /// </summary>
        public async Task<bool> FileQuitAction()
        {
            bool returnValue = false;

            try
            {
                //use cleanup logic from Window_Closed here
                returnValue = await ViewModel.FileExit();
                //probably need to use this too upon affirmative response
                if (returnValue)
                {
                    await DisposeSettings();

                    //3) Qml ignores result, so scram
                    QCoreApplication.Quit();
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(string.Format("Exception: {0}", ex.Message));
            }
            return returnValue;
        }
        #endregion Actions

        #endregion Methods

        #endregion ViewModel

        #region Methods
        #region utility
        // /// <summary>
        // /// Methods can return .NET types.
        // /// The returned type can be invoked from Qml (properties/methods/events/etc), 
        // ///  even if it has not been registered.
        // /// Not used in this implementation.
        // /// </summary>
        // /// <returns></returns>
        // public MvcView CreateNetObject()
        // {
        //     return new MvcView();
        // }

        // private async Task WaitDialogFree(DialogInfoBase<object, string> dialogInfo)
        // {
        //     //Console.WriteLine("WaitDialogFree,start,Busy:" + dialogInfo.Busy.ToString());
        //     while (dialogInfo.Busy)
        //     {
        //         await Task.Delay(1000);
        //         //Console.WriteLine("WaitDialogFree,loop,Busy:" + dialogInfo.Busy.ToString());
        //     }
        //     //Console.WriteLine("WaitDialogFree,end,Busy:" + dialogInfo.Busy.ToString());
        // }
        #endregion utility
        #endregion Methods
        #endregion Qml.Net
    }
}
