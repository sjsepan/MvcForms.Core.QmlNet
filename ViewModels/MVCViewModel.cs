﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Ssepan.Utility.Core;
using Ssepan.Application.Core;
using Ssepan.Application.Core.QmlNet;
using MvcLibrary.Core;

namespace MvcForms.Core.QmlNet
{
	/// <summary>
	/// Note: this class can subclass the base without type parameters.
	/// </summary>
	public class MVCViewModel :
        FormsViewModel<string, MVCSettings, MVCModel, MvcView/*object*/>
    {
        #region Constructors
        public MVCViewModel() { }//Note: not called, but need to be present to compile--SJS

        public MVCViewModel
        (
            PropertyChangedEventHandler propertyChangedEventHandlerDelegate,
            Dictionary<string, string> actionIconImages,
            FileDialogInfo<object, string> settingsFileDialogInfo
        ) :
            base(propertyChangedEventHandlerDelegate, actionIconImages, settingsFileDialogInfo)
        {
            try
            {
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        public MVCViewModel
        (
            PropertyChangedEventHandler propertyChangedEventHandlerDelegate,
            Dictionary<string, string> actionIconImages,
            FileDialogInfo<object, string> settingsFileDialogInfo,
            MvcView/*object*/ view
        ) :
            base(propertyChangedEventHandlerDelegate, actionIconImages, settingsFileDialogInfo, view)
        {
            try
            {
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// model specific, not generic
        /// </summary>
        internal void DoSomething()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Doing something...",
                    null,
                    null, //_actionIconImages["Xxx"],
                    true,
                    33
                );

                ModelController<MVCModel>.Model.SomeBoolean = !ModelController<MVCModel>.Model.SomeBoolean;
				ModelController<MVCModel>.Model.SomeInt++;
                ModelController<MVCModel>.Model.SomeString = DateTime.Now.ToString();

                ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean = !ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean;
				ModelController<MVCModel>.Model.SomeComponent.SomeOtherInt++;
                ModelController<MVCModel>.Model.SomeComponent.SomeOtherString = DateTime.Now.ToString();

                ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean = !ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean;
				ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherInt++;
                ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherString = DateTime.Now.ToString();
                //TODO:implement custom messages
                UpdateStatusBarMessages(null, null, DateTime.Now.ToLongTimeString());

                ModelController<MVCModel>.Model.Refresh();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
            finally
            {
                StopProgressBar("Did something.");
            }
        }

        /// <summary>
        /// get color RGB
        /// </summary>
        public async Task GetColor()
        {
            ColorDialogInfo<object, string, string> colorDialogInfo = null;
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Color" + ACTION_IN_PROGRESS,
                    null,
                    null, //_actionIconImages["Xxx"],
                    true,
                    33
                );
                colorDialogInfo = new ColorDialogInfo<object, string, string>
                (
                    parent: View,
                    modal: true,
                    title: "Select Color",
                    response: Button_None,
                    color: default
                );

                colorDialogInfo = await DialogsController.Dialogs.Instance.GetColor(colorDialogInfo);

                if (!colorDialogInfo.BoolResult)
                {
                    throw new ApplicationException(string.Format("GetColor: {0}", colorDialogInfo.ErrorMessage));
                }

                if (colorDialogInfo.Response != Button_None)
                {
                    if (colorDialogInfo.Response == Button_OK)
                    {
                        UpdateStatusBarMessages
                        (
                            StatusMessage +
                            string.Format
                            (
                                "{0}",
								colorDialogInfo.Color
							) +
                            ACTION_IN_PROGRESS,
                            null
                        );
                    }
                    else
                    {
                        //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if dialog was cancelled
                    }
                }
                else
                {
                    //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if dialog was cancelled
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        /// <summary>
        /// get font name
        /// </summary>
        public async Task GetFont()
        {
            FontDialogInfo<object, string, string> fontDialogInfo = null;
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Font" + ACTION_IN_PROGRESS,
                    null,
                    null, //_actionIconImages["Xxx"],
                    true,
                    33
                );
                fontDialogInfo = new FontDialogInfo<object, string, string>
                (
                    parent: View,
                    modal: true,
                    title: "Select Font",
                    response: Button_None,
                    fontDescription: default
                );

                fontDialogInfo = await DialogsController.Dialogs.Instance.GetFont(fontDialogInfo);

                if (!fontDialogInfo.BoolResult)
                {
                    throw new ApplicationException(string.Format("GetFont: {0}", fontDialogInfo.ErrorMessage));
                }

                if (fontDialogInfo.Response != Button_None)
                {
                    if (fontDialogInfo.Response == Button_OK)
                    {
                        UpdateStatusBarMessages(StatusMessage + fontDialogInfo.FontDescription + ACTION_IN_PROGRESS, null);
                    }
                    else
                    {
                        //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if dialog was cancelled
                    }
                }
                else
                {
                    //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if dialog was cancelled
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }
        #endregion Methods

    }
}
