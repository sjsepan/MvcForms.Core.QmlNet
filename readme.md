# Desktop GUI app demo, on Linux, in C# / DotNet[5+] , Qt / Qml.Net / QuickControls2, using VSCode; requires ssepan.utility.core, ssepan.io.core, ssepan.application.core, ssepan.application.core.qmlnet, mvclibrary.core

## About

Although this was working when initially posted, after updating the Qml.Net packages, it no longer works. The author of Qml.Net indicated that they no longer had time for that project. I will run the analyzer, and make sure it compiles, and then I will shelve the project.

![MvcForms.Core.QmlNet.png](./MvcForms.Core.QmlNet.png?raw=true "Screenshot")

### Usage notes

~This application uses keys in app.config to identify a settings file to automatically load, for manual processing and saving. 1) The file name is given, and the format given must be one of two that the app library Ssepan.Application.Core knows how to serialize / deserialize: json, xml. 2) The value used must correspond to the format expected by the SettingsController in Ssepan.Application.Core. It is specified with the SerializeAs property of type SerializationFormat enum. 3) The file must reside in the user's personal directory, which on Linux is /home/username. A sample file can be found in the MvcLibrary.Core project in the Sample folder.
~The automatic loading is not necessary (it is a carry-over from the original console app) and can be disabled by editing the code in InitViewModel() in MvcView.qml.cs to remove the ViewModel.FileOpen() call.

### Instructions for downloading/installing .Net 'Core'

<https://dotnet.microsoft.com/download>
<https://docs.microsoft.com/en-us/dotnet/core/install/linux?WT.mc_id=dotnet-35129-website>

### Instructions/example of adding Qml.Net to dotnet

(from Qml.Net qmlnet.github.io)
dotnet add package Qml.Net --version 0.12.0-alpha.3
dotnet add package Qml.Net.WindowsBinaries
dotnet add package Qml.Net.OSXBinaries
dotnet add package Qml.Net.LinuxBinaries --version 0.12.0-alpha.3
See also
<https://qmlnet.github.io/setup/managed-hosting/>

Note: I added only the general package and the Linux-specific one in this project
Note for Linux users: Package libc6-dev is required to be installed because it contains libdl.so that is needed.

ex: "sudo apt-get install libc6-dev"

### Install package for app configuration

~in folder for project use:
dotnet add package System.Configuration.ConfigurationManager

### Creating app ui in Qt Designer

Note: this creates a .ui file; there is no indication of a way to use it in Qml.Net

### Creating app ui in Qt Creator

Note: this creates a .qml file; but once loaded, there are no widget toolboxes appearing with controls to use.
Update: To fix this problem, you need to ensure that the syntax is correct in your code editor, then comment out the import statements for your custom .Net objects, and then save. Address any other issues raised by Qt Creator, and the controls should re-appear in the toolbox, and you should be able to see the design-mode of the form. After completing the visual-editing, you may return to you code-editor and uncomment elements not recognized by Qt Creator.

### Creating app UI in VSCode

Edit .qml manually using QtQuick2 (and QtQuick1 where necessary), and use Qt Assistant app from Qt Creator package as a reference for controls.

### GUI tutorial for Qml.Net/CSharp\#

<https://github.com/qmlnet/qmlnet>

### Registering .Net Objects

<https://qmlnet.github.io/features/registering-net-objects/>

### Menu / Toolbar and Actions

The general pattern is for Qml menu items and toolbar buttons to have their onTriggered / onClicked handlers call into .Net code.actionName(), which will perform not only the desired action but also manage the status / error / progress / status-icons.

### Signals and Binding

Binding is possible by using the NotifySignal attribute on individual properties. However, in order to be more consistent with my other demonstration projects, I will use the Signal attribute on the properties' class, and a switch statement in Qml to explicitly handle notifications in the UI.

### Displaying Qt dialogs with .Net code

Qml.Net exposes some objects to .Net, but there are many that it seems it does not. Dialogs fall into that category, and in order to present Qt dialogs, one has to do it in Qml. Since my design involves primarily writing the code-behind in C#, it is largely stymied by the inability to access Qt dialogs directly. However, it is possible to define a signal for the purpose of opening the dialog in a Qml-based handler. Since there are a lot of parameters for a dialog, it will be convenient to make use, here in this prototype project, of dialog info classes that I have created for the more elaborate demo projects that follow after the prototype. These dialog info classes can be exposed to Qml as properties of the .Net code class. These will also serve another important purpose -- to include those properties which need to be passed back from the dialog to the .Net code. Furthermore, these properties will include a semaphore which will be awaited by the .Net code that triggered the signal. The .Net code will then proceed based on the dialog inputs.

### Handling onClosing event

The onClosing handler of ApplicationWindow is a special case, because it needs to set a flag if cancelling is desired, and it gets this back from the .Net code, after that triggers a dialog and then sees the result. The async call to .Net code is done with Qml.Net's Net.await() call, which uses an anonymous method as a call-back when the await completes. All if this would be just fine, but only the code inside the call-back is awaited. Code outside, in the onClosing handler, after the await, is not awaited. Instead, it appears that the remaining code executes, and then the handler does not return until the await completes. This results in the close.accepted being set before the awaited result is available. And putting the assignment inside the call-back is just as bad; it is ignored and is as though it is never set. (The default is then true, and the window closes.) The work-around is to conditionally scram the app from within the .Net code with QCoreApplication.Quit();

### Issues

-Cannot access static .Net Dialogs class or members, and cannot store instance of Dialogs in qml between signals (OnOpenXxxDialog, OnAccepted/OnRejected). Using Singleton pattern is suggested for C++ users of Qml, but implementing in C# is less clear. Ended up using a DialogsController class wrapped around the Dialogs class, and a GetNetObject method to hide static nature from Qml.
<https://github.com/qmlnet/qmlnet/issues/78>

-"QML DefaultDialogWrapper: Binding loop detected for property "x""
Move "import QtQuick.Dialogs 1.3" before "import QtQuick.Controls 2.3"
<https://bugreports.qt.io/browse/QTBUG-62023>

-multiple (4) "file:///home/username/.qmlnet-qt-runtimes/qt-5.15.1-7fc8b10-linux-x64/qt/qml/QtQuick/Controls/TableViewColumn.qml:66:1: QML TableViewColumn: Accessible must be attached to an Item"
Appeared after adding FileDialog to working project; seems to be related to:
<https://bugreports.qt.io/browse/QTBUG-86315> 5.15.0
<https://bugreports.qt.io/browse/QTBUG-86890> 5.15.1
Per this SO, it may also be related to version of PyQt in use (inside Qml.Net? I'm writing C# but SO also mentions FileDialog):
<https://stackoverflow.com/questions/65116150/how-to-fix-qml-tableviewcolumn-accessible-must-be-attached-to-an-item> 5.15.1

-after adding FileDialog, internal Qt error appearing unless user changes window frame size (at all):
"Model size of -56 is less than 0"
<https://bugreports.qt.io/browse/QTBUG-72543>
<https://stackoverflow.com/questions/54819099/qml-tableviewcolumn-quick-control-1-alert-model-size-of-2-less-than-0>

-error "Couldn't ready block" during RuntimeManager.DiscoverOrDownloadSuitableQtRuntime(); seems to be a timeout of the download when running in the integrated terminal
run

```
cd ~/.qmlnet-qt-runtimes/
wget https://github.com/qmlnet/qt-runtimes/releases/download/releases/qt-5.15.1-7fc8b10-linux-x64-dev.tar.gz
tar -xvf qt-5.15.1-7fc8b10-linux-x64-dev.tar.gz
```

Then it gets "~/Projects/DotNetCoreProjects/MvcForms.Core.QmlNet/bin/Debug/net8.0/libdl: cannot open shared object file: No such file or director". Note that libc6-dev is already installed.

### Print Dialog

Although there is a QPrintDialog in Qt, there does not appear to be a print dialog available in Qml and Qml.Net. This prototype will include some unused or commented-out code for future use if such a dialog becomes available.

### Enhancements

0.10:
~refactor to newer C# features
~refactor to existing language types
~perform format linting
~redo command-line args and how passed filename overrides config file settings, including addition of a format arg.

0.9:
~Initial release.

### Contact

Steve Sepan
<sjsepan@yahoo.com>
3/6/2024
